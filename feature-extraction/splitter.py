import scipy.io.wavfile as wav
import sys

def split_audio(in_name, init_sec, end_sec, desv, out_name):
	print("From " + in_name + " extracting " + out_name)
	(rate, sig) = wav.read(in_name)
	wav.write(out_name+".wav", rate, sig[rate*init_sec+desv:rate*end_sec+desv])

if len(sys.argv) != 6:
	print("Mal formato. Debe ser in_name init_sec end_sec desv out_name")
else:
	split_audio(str(sys.argv[1]), int(sys.argv[2]), int(sys.argv[3]), int(sys.argv[4]), str(sys.argv[5]))