from python_speech_features import mfcc
import scipy.io.wavfile as wav
import os
import time
import sys

def write_csv_line(f, array):
    i = 0
    array = array.tolist()[0]
    while i + 1 < len(array):
        f.write(str(array[i]))
        f.write(",")
        i = i + 1
    f.write(str(array[len(array) - 1]))
    f.write("\n")
    
def extract_features(name, rate_pond):
    ts = time.time()
    
    print("Extracting from " + name)
    (rate, sig) = wav.read(name)
    
    rate = int(rate_pond * rate)
    
    print("Using " + str(rate) + " frames per vector")
    
    i = 0
    wlen = 100
    
    csv_name = name.split('/')
    if(len(csv_name) != 1):
        csv_name = csv_name[len(csv_name) - 1]
    else:
        csv_name = csv_name[0]
        csv_name = csv_name.split('.')[0]
    
    f  = open(csv_name + ".csv", "w")
    f.write("")
    
    while (i + 1) * rate <= sig.shape[0]:
        if(i%10 == 0):
        	print(i)

        init = i*rate
        end = init + rate
        try:
            sample = sig[init:end, :]
        except IndexError:
            print("exception")
            sample = sig[init:end]
        mfcc_feat = mfcc(sample, rate, winlen=wlen, nfft=rate*wlen)
        write_csv_line(f, mfcc_feat)
        i = i + 1

    f.close()

    ts2 = time.time()
    print("Total time (seg): " + str(ts2-ts))
    
    print("OK")

def extract_all(rate_pond):
    path = "audios/wav/"
    for filename in os.listdir(path):
    	extract_features(path+filename, rate_pond)

if(len(sys.argv) < 2):
	print("Mal formato. Debe ser in_name sec_sample")
elif(len(sys.argv) == 2):
	extract_all(float(sys.argv[1]))
else:
	extract_features(str(sys.argv[1]), float(sys.argv[2]))
