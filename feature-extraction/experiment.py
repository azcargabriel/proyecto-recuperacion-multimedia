import pandas as pd
import numpy as np
import scipy as sc
import operator
from scipy.spatial.distance import euclidean

#Clase Vote, representa un voto a un frame
#Contiene el valor del frame y la cantidad de votos q
class Vote:
    value = 0
    q = 1
    
    def __init__(self, value):
        self.value = value
        self.q = 1
        
    def add_one(self):
        self.q = self.q + 1
        
    def __str__(self):
        return str(int(self.value/60)) + ":" + str(self.value%60) + " votos: " + str(self.q)

#Clase Best, representa un frame que es de los mejores en distancia
#Contiene el valor del frame (en segundos) y la distancia
class Best:
    time = 0
    distance = 0
    
    def __init__(self, t, d):
        self.time = t
        self.distance = d
    
    def __str__(self):
        return str(int(self.time/60)) + ":" + str(self.time%60) + " - Dist: " + str(self.distance)

#Clase Ans, guarda los mejores 3 Best de la comparacion entre un vector y los capitulos
#Contiene el largo y el array
class Ans:
    def __init__(self):
        self.array = []
        self.len = 0

    def add(self, b):
        if self.len == 0 or self.len == 1 or self.len == 2:
            self.array.append(b)
            self.len = len(self.array)
        else:
            self.array[self.len - 1] = b
        self.array.sort(key=operator.attrgetter('distance'))
    
    def print_array(self):
        for i in self.array:
            print (i)
        print("\n")

#Agrega un voto a un Vote en un arreglo, si no lo agrega
def add_vote(array, value):
    if array != None:
        for i in array:
            if i.value == value:
                i.add_one()
                return array
    array.append(Vote(value))
    return array
    

f = pd.read_csv("navidad.csv", header=None)
f2 = pd.read_csv("navidad-corte.csv", header=None)

global_indexs = []

i = 0
i2 = 0

maxi2 = f2.shape[0]
maxi = f.shape[0]

while i2 < maxi2:
    actualMin = 10000000
    actualMinIndex = 0
    i = 0
    indexs = Ans()
    while i < maxi:
        d = euclidean(np.array([f.loc[i, : ]]), np.array([f2.loc[i2, : ]]))
        if d < actualMin:
            actualMin = d
            actualMinIndex = i
            indexs.add(Best(actualMinIndex, actualMin))
            actualMin = indexs.array[index.len - 1]
        i = i + 1
    global_indexs.append(indexs)
    i2 = i2 + 1
    
print(global_indexs)    

vs = []
for i in global_indexs:
    for x in i.array:
        vs = add_vote(vs, x.time)
    i.print_array()

vs.sort(key=operator.attrgetter('q'))
for v in vs:
    print(v)







